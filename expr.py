import xml.etree.ElementTree as ET
import subprocess
import os
import csv
import time
import math as m

KB = 1024
MB = KB * KB
GM = KB * MB

GHz = 1e+09

def get_count(count_dict, key_portion, printt=False):
  count = 0
  #print("Key (", key_portion, "): ", sep='', end='')
  for k, v in count_dict.items():
    if key_portion.lower() in k.lower():
      count += v
      #if printt: print(v, "+", end='')
  #print("0 : OUT")
  return count

def get_list(count_dict, key_portion):
  lst = []
  for k, v in count_dict.items():
    #print(k)
    if key_portion.lower() in k.lower() and not m.isinf(v):
      lst.append((k, v))
  return lst
  
def get_read_and_write_rates(results_dict, cache_type='L1V'):
  read_hit_count = get_count(results_dict['read-hit'], cache_type, printt=True)
  read_miss_count = get_count(results_dict['read-miss'], cache_type)
  read_mshr_count = get_count(results_dict['read-mshr-hit'], cache_type)
  
  write_hit_count = get_count(results_dict['write-hit'], cache_type)
  write_miss_count = get_count(results_dict['write-miss'], cache_type)
  write_mshr_count = get_count(results_dict['write-mshr-hit'], cache_type)

  return ((read_hit_count, read_miss_count, read_mshr_count), (write_hit_count, write_miss_count, write_mshr_count))

def get_cu_CPI(results_dict, cache_type='L1V'):
  CPI = get_list(results_dict['cu_CPI'], cache_type)
  cpi_count = 0
  for cpi in CPI: cpi_count += cpi[1]
  return cpi_count / len(CPI)

def get_simd_CPI(results_dict, cache_type='L1V'):
  CPI = get_list(results_dict['simd_CPI'], cache_type)
  #print("FOR", cache_type, ":", CPI)
  cpi_count = 0
  for cpi in CPI: cpi_count += cpi[1]
  return cpi_count / len(CPI)


class CacheProperties:
    def __init__(self, name):
        self.name = name
        self.bankLatency = 60
        self.numBanks = 1
        self.log2BlockSize = 6 # TODO : this property seems to actually be controlled by the 'shader array builder' and the not cache builder directly
        self.wayAssociativity = 4
        self.numMSHREntry = 16
        self.totalByteSize = 16 * KB
        self.victimfinder = "lru"


    def to_xml(self):
        root = ET.Element(self.name)
        for k, v in self.__dict__.items():
            if k == "name": continue
            name = ET.SubElement(root, k)
            name.text = str(v)
        return root


    def WithFreq(self, freq):
        self.frequency = freq
        return self
    def WithBankLatency(self, lat):
        self.bankLatency = lat
        return self
    def WithNumBanks(self, num_banks):
        self.numBanks = num_banks
        return self
    def WithLog2BlockSize(self, block_size):
        self.log2BlockSize = block_size
        return self
    def WithWayAssociativity(self, way_assoc):
        self.wayAssociativity = way_assoc
        return self
    def WithNumMSHREntry(self, num_mshr_entries):
        self.numMSHREntry = num_mshr_entries
        return self
    def WithTotalByteSize(self, total_byte_size):
        self.totalByteSize = total_byte_size
        return self
    def WithVictimFinder(self, finder):
        self.victimfinder = finder
        return self


def fir_generator():
    for length in [4096*128*4]: #[4096, 8192, 8192*2]:
        yield ('fir', length), f".\\fir.exe -length={length} -timing -parallel=true --report-all"
def matmult_generator():
    for size in [1024*2]: #[128, 2048, 4096]:
        yield ('matmult', size), f".\\matrixmultiplication.exe -x={size} -y={size} -z={size} -timing --report-all"

def stencil_generator():
    for size in [(2*1024, 2)]: #[128, 2048, 4096]:
        yield ('stencil', size), f".\\stencil2d.exe -row={size[0]} -col={size[0]} -iter={size[1]} -timing --report-all"

block_sizes = [6] #[5, 6, 7, 8]
eviction_policies =  ["lru", "mru", "random", "lfu", "mfu", "lruk"]
set_associativity = [2, 4, 8] #[2, 4, 8, 16]
cache_sizes = [16*1024] #[2*1024, 4*1024, 8*1024, 16*1024, 32*1024]
# TODO: if we dont get good results, "OH WELL!"

tests = [
    #("matrixmultiplication", matmult_generator()),
    ("stencil2d", stencil_generator()),
    #("fir", fir_generator()),
    ]

global_test_results = dict()

for test_name, call_func_gen in tests:
    global_test_results[test_name] = dict()
    test_results = global_test_results[test_name] # REFERNCE ASSIGNMENT

    # Change the working directory of the Python process

    print("Building... ", end='')
    os.chdir(test_name)
    # Build the program using "go build"
    subprocess.call("go build", shell=True)
    print("Complete!")
    for test_id, test_call_func in call_func_gen:
        for block_size in block_sizes:
            for cache_size in cache_sizes:
                for sa in set_associativity:
                    for finder in eviction_policies:
                        test_key =  (block_size, cache_size, finder, sa, test_id)
                        print("Running: ", test_key)
                        
                        cache_props = [
                            CacheProperties("OverallCacheLineSize").WithLog2BlockSize(block_size),
                            CacheProperties("L1VCache").WithLog2BlockSize(block_size).WithVictimFinder(finder).WithWayAssociativity(sa).WithTotalByteSize(cache_size),
                            #CacheProperties("L1ICache").WithLog2BlockSize(block_size).WithVictimFinder(finder).WithWayAssociativity(sa).WithTotalByteSize(cache_size),
                            #CacheProperties("L1SCache").WithLog2BlockSize(block_size).WithVictimFinder(finder).WithWayAssociativity(sa).WithTotalByteSize(cache_size)
                        ]

                        root = ET.Element("root")
                        for c in cache_props:
                            root.append(c.to_xml())

                        tree = ET.ElementTree(root)
                        ET.indent(tree, space = "    ")
                        tree.write("..\\cacheData.xml")


                        start_time = time.time()

                        subprocess.call(test_call_func, shell=True)

                        end_time = time.time()

                        execution_time = end_time - start_time

                        print(f"Execution time: {execution_time:.2f} seconds")

                        metrics = dict()
                        metrics['total_time'] = dict()
                        metrics['kernel_time'] = dict()
                        metrics['simd_CPI'] = dict()
                        #metrics['simd_inst_count'] = dict()
                        metrics['cu_CPI'] = dict()

                        metrics['write-hit'] = dict()
                        metrics['write-miss'] = dict()
                        metrics['write-mshr-hit'] = dict()

                        metrics['read-hit'] = dict()
                        metrics['read-miss'] = dict()
                        metrics['read-mshr-hit'] = dict()

                        # Open the CSV file in read mode
                        with open('metrics.csv', 'r') as csvfile:
                            # INDEX, where, what, value
                            
                            # Create a CSV reader object
                            csvreader = csv.reader(csvfile)

                            # Iterate over each row in the CSV file
                            #i = 0
                            for row in csvreader:
                                for key in metrics.keys():
                                    if (" " + key) in row:
                                        metrics[key][row[1]] = float(row[3].strip())

                        test_results[test_key] = metrics
                        #print("Done:", metrics)
    # After the test, leave the directory
    os.chdir("..")





# OUTPUT RESULTS IN NICE CLEAN FORMS
for global_key, test_results in global_test_results.items():
    for k in test_results.keys():
        read_rates_v, write_rates_v = get_read_and_write_rates(test_results[k], 'l1v')
        read_rates_i, write_rates_i = get_read_and_write_rates(test_results[k], 'l1i')
        read_rates_s, write_rates_s = get_read_and_write_rates(test_results[k], 'l1s')

        cu_cpi = get_cu_CPI(test_results[k], 'cu')
        simd_cpi = get_simd_CPI(test_results[k], 'cu')

        test_results[k]['simd_CPI'] = {'gpu' : simd_cpi}
        test_results[k]['cu_CPI'] = {'gpu' : cu_cpi}

        test_results[k]['write-hit'] = {'L1V' : write_rates_v[0], 'L1I' : write_rates_i[0], 'L1S' : write_rates_s[0]}
        test_results[k]['write-miss'] = {'L1V' : write_rates_v[1], 'L1I' : write_rates_i[1], 'L1S' : write_rates_s[1]}
        test_results[k]['write-mshr-hit'] = {'L1V' : write_rates_v[2], 'L1I' : write_rates_i[2], 'L1S' : write_rates_s[2]}

        test_results[k]['read-hit'] = {'L1V' : read_rates_v[0], 'L1I' : read_rates_i[0], 'L1S' : read_rates_s[0]}
        test_results[k]['read-miss'] = {'L1V' : read_rates_v[1], 'L1I' : read_rates_i[1], 'L1S' : read_rates_s[1]}
        test_results[k]['read-mshr-hit'] = {'L1V' : read_rates_v[2], 'L1I' : read_rates_i[2], 'L1S' : read_rates_s[2]}

    # print("Printing:\n", test_results)
    #with open(f'test_results_stencil_rq1_{global_key}.txt', 'w') as file:
    #    file.write(str(test_results))
    with open(f'test_results_stencil_rq2_{global_key}.txt', 'w') as file:
        file.write(str(test_results))
    #print("\n\n\n\n\n")
    #for k, v in test_results.items():
    #    print(k, ":", v)

'''
-Speed
-molecular dynamic
-Tag vs swap on ptr (del and new)
    - move on a grid or graph?
        - actually very smart, for a grid we get a pattern
        - might be same for molecular dynamics
- graph
    - as SOA

- optim
    - if very general
        - can be included in OS layer
            - I am slightly doubtful?
                - but it would be very novel?

REAL BENCHMARK?


TODO:
    - ARC 
    - write blurbs for ecs, 
        - make some figure for the empty page
    - make a graph which explains SOEMTHING!!!
        - hit the RQ1, clownch

    - taxes

    - study for OS
    - write for ECS
    - write for MGPU


- The two queues concat together will always be the same size when we care about evicting
    - equal to way assoc
- What queue will get added to is vital?
- Or we could ignore that and simply try to maintain some marker without that understanding?
    - 'I want to maintain the size of T1 or T2 at *THIS NUMBER*'?????

However long?
    - 


    Questions:
        - different levels of abstraction
            - how would that work, high level vs low level, difficulties????????
            - other ways to think about these different abstraction layers beyond high vs low level?
                - clear mapping of higher level mapping to some portion of a lower level
        - Destruction of reasons for DSL, easier compiler tricks are dissolved??
        - 


        2811 + 768 + 517

*Lena Zheng, *Grishma Baruah, *Nick Brown, *Nicolett Glut, *Cheyenne Hwang,
*Min Kim, and Nathan Goldberg

1)
True
True
False
True
False
True
True
False

2) All yes, exception NP under complement, which is open


Compare of AFS and LBFS
Adaptive binary translation???

AFS:
    - Vice : Homogeneous, location-transparent file namespace to all clients, with a group of trustworthy servers known as Vice
    - Venus: caches files from Vice and returns updated versions to the servers, only when a file is opened or closed does Venus communicate

    The following are the server and client components used in AFS networks:
        Any computer that creates requests for AFS server files hosted on a network qualifies as a client.
        The file is saved in the client machine’s local cache and shown to the user once a server responds and transmits a requested file.
        When a user visits the AFS, the client sends all modifications to the server via a callback mechanism. The client machine’s local cache stores frequently used files for rapid access.

LBFS:
    - Still distritubed
    - On both the client and server, LBFS must index a set of files to recognize data chunks it can avoid sending over the network.
    - "After a client has written and closed a file, another client opening the same file will always see the new contents."
        - I think the same as AFS?
    - LBFS uses a large, persistent file cache at the client. LBFS assumes clients will have enough cache to contain a user’s entire working set of files
    - KEY DIFFERENCE : it transfers on modifications "since in our model, the client might crash"

    - Rsync:
        - First, the recipient, B, breaks its file F0 into non-overlapping, contiguous, 
        fixed-size blocks. B transmits hashes of those blocks to A. 
        A in turn begins computing the hashes of all (overlapping) blocks of F (great for shifts). 
        If any of those hashes matches one from F0, A avoids sending the corresponding 
        sections of F, instead telling B where to find the data in F0

    -

Exokernel:
    - As little abstraction as possible
    - A realization of the 'end-to-end' argument for hardware-software interfacing
    - Aegis:
        - An exokernel implementation
            - Secure bindings
            - Memory:
                - Capabilities : a program can share a page with another program by sending it a capability to access the page
                    - kernel ensures accesses are valid
            - Networking:
                - packet filter for organizing packets, download bytecode designed for easy security-checking
            - 

Capricio: easy

MapReduce:
    - Map
    - Reduce
    - Combine

    map(String key, String value):
    // key: document name
    // value: document contents
    for each word w in value:
    EmitIntermediate(w, "1");

    reduce(String key, Iterator values):
    // key: a word
    // values: a list of counts
    int result = 0;
    for each v in values:
    result += ParseInt(v);
    Emit(AsString(result));

    On map failures, maps get reallocated and computed
    On reduce failures, we dont need to recompute,since they are stored on a global file system
    Counters are a thing


Chubby:
    - Use consensus protocol to create a super server that never fails
        - we can use it for a lot of applications which rely on faultless servers
    - Sessions are maintained betwee chubby clients and a chubby cell by 'KeepAlives'
    - When a master loses mastership
        - It discards its in-memory state about sessions and locks
        - lease timer is stopped until new master

    - Once a client has contacted the new master, 
    the client library and master co-operate to provide the illusion to 
    the application that no failure has occurred.

TaintDroid:
    - Track JVM
    - VM interpretter attach
    - Taint Tag Storage:
        - method local variables, method arguments, class static fields, class instance fields, arrays
            - in all cases, a 32-bit bitvector with each variable to encode the taint tag
            - this allows 32 different taint markings

    - Propogate based on setting and use
        - native code cannot be managed

x86 Virtualization:
    - 
Hardware vs software virtualization:
    - Classical VMM
        - uses trap and enumlate
        - not poossible for x86
            - visiiblity of privleged state, lack of traps for privileged instruction at user level

    - Software VMM
        - execute guest on Interpreter: fetch-decode-execute in SOFTWARE, terrible!
        - Binary tranlation: On demand, translate guest code to executed code
        - Adaptive: translated code is dajusted to response to huest behavior change

    - Hardware VMM
        - architectural changes to permit classical virtualization on x86
            - new less privileged execution mode - guest mode, supports directrion xec of guest code and rivlege code

        - new instructiion: vmrun, vmexit
            - transfers from host to guest mode, loading guest state from VMCB and continues exec
            - transfers from guest to host mode
 

    - Adaptive binary translation addresses this inefficiency by dynamically optimizing the translation process based on the behavior of the guest code. As the guest code is executed, the translation process is updated to better match the guest code's execution pattern, resulting in faster and more efficient emulation. 
    For example, if a particular guest code sequence is frequently executed, the adaptive binary translation system may generate a specialized version of the translation that is optimized for that sequence, rather than using a generic translation for every execution. This can significantly improve performance by reducing the overhead of translation.
    Overall, adaptive binary translation is a powerful technique for improving the performance of virtualization by dynamically optimizing the translation process to match the behavior of the guest code.


Basic blocks:
    - a single out, as many ins as can
CFG: relationship for basic blocks
Dominator: Basic block that must be passed through to reach some block
Loop: a SCC cycle with a single entry
Instruction Level DAGs are easy
    - Fact(*) <-- Fact0, <-- I0
IN[n]: instructions that could affect the block
OUT[n]: instructions that could affect next blocks
GEN[n] : the set of all variables read by n
KILL[n]: a singleton set containing the variable that is written by n
USES[n]: what variables are used
  DU[i]: upward exposed uses of variable i in all basic blocks
    - i.e. what instructions will use the SET for the instruction
Constant propogation by steppies
stride analysis: be smart

Live Variable:
    OUT[n] = 0 for each CFG node n
    change = true
    While (change) 
        1. For each n other than ENTRY and EXIT
            OUTold[n] = OUT[n]
        2. For each n other than ENTRY
            IN[n] = union of OUT[m] for all predecessors m of n
        3. For each n other than ENTRY and EXIT
            OUT[n] = ( IN[n] – KILL[n] ) È GEN[n]
        4. change = false
        5. For each n other than ENTRY and EXIT
            If (OUTold[n] != OUT[n]) change = true

Loop Invariant:
s is loop invariant if both b and c satisfy one of the following
  – it is constant
  – all definitions that reach it are from outside the loop
  – only one definition reaches it and that definition is also  loop invariant

We can move a loop invariant statement a = b op c if
  – The statement dominates all loop exits where a is live
  – There is only one definition of a in the loop
  – a is not live before the loop

--GunRock
    - Advance, Filter, Compute
    - Advance -> Compute -> Filter
    - Workload mapping:
    - Push vs pull
        - DFS-style vs DFS-style?

--VeGen
    - Lane level parallelism
    - Pattern matching
    - Vector packs
    - Selection
        - Beam search
    - Simple lowering generation

--MLIR
    - Abstraction levels
    - Attributes, ops, variables, type safe, 

--HOTL: a higher order theory of locality
    - FILMR : 
        -footprint (data in a time window), 
            - Let W be the set of all length-l windows in a length-n trace
            - fp(l) = Σ_all_w_of_length_l fp_w / (n - l + 1)
        -inter-miss time (the time between two data block accesses), 
            - im(c) = vt(c + 1) - vt(c)
        -volume fill time (time it takes to access a volume of data, inverse of footprint?), 
            - vt(c) = fp^(-1)(c)
        -miss rate, 
            - (fp(x + Δx) - fp(x)) / Δx
        -reuse distance (the amount of data accessed between two accesses to the same data)
            - P(rd = c) = mr(c - 1) - mr(c), probility that reuse distance is cache capacity

--Tigr: Transforming Irregular Graphs for GPU-Friendly Graph Processing.
    - Regular and iiregular graphs
    - T_cliq: a split where all new nodes connect to all new nodes
    - T_circ: a split where the new nodes connect in a circle
    - T_star: a plit where the new nodes are pointed to by a single, center node
        - Their solution is to recursively apply T_star to hub node until degree drops to K
        - Uniform-Degree Tree: 
            - Solves issues where hub node of T_star makes more residual nodes than needed
            - Maintains a queue of split nodes to connect, If the queue has more than K node, a new node is created, and connected to K nodes popped from the queue, The new node is appended to the queue, Iterates until the queue has no more than K nodes
    - Virtual graphs: for programming model, saves storage and allows better striding by virtualizating data
        - Uses Virtual node array which points to position in global edge lists
        - Uses striding to better optmize cache

--LLVM: A Compilation Framework for Lifelong Program Analysis & Transformation
    - LLVM also makes the Control Flow Graph (CFG) of every function explicit in the representation. 
    A function is a set of basic blocks, and each basic block is a sequence of LLVM instructions, 
    ending in exactly one terminator instruction (branches, return, unwind, or invoke; the latter two are explained later below). 
    Each terminator explicitly specifies its successor basic blocks.
    - The LLVM ‘cast’ instruction is used to convert a value of one type to another arbitrary type, and is the only way to perform such conversions.
    - getelementptr: 
        - For example, the C statement “X[i].a = 1;” translated into LLVM instructions:
        - %p = getelementptr %xty* %X, long %i, ubyte 3;
        - store int 1, int* %p;
            - where we assume a is field number 3 within the structure X[i], and the structure is of type %xty.
    - Compile time, link time, runtime, optim time, etc?
    - malloc, free, alloca
    - call instruction takes a typed function, invoke, unwind
    - Compiler Frontend, Linker, Native code, Offline optimizer do profile driven optim
    - Langauges which reuquire sophisticated runtime systems might not beenfit (Java garbage colllection?)




TODO:
    - push MGPU code
    - make stencil data
        - RQ1 and RQ3 cooking now
        - RQ2 cooking later: might be good to make test graph, then hit bigger later
    - make graphs for RQ1 and RQ3 for stencil and matmult
    - write about RQ1 and RQ3
    - make graphs for RQ2 for matmult and stencil
    - write about RQ2

    - fix ECS code
    - push ECS code
    - polish ECS




'''
