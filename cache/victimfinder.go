package cache

import (
	"fmt"
	"math"
	"math/rand"
	"time"
)

// A VictimFinder decides with block should be evicted
type VictimFinder interface {
	FindVictim(set *Set) *Block
	Reset(sets []Set, numSets int, numWays int)
	NotifyVictim(block *Block)
	NotifyVisit(block *Block)
	MakeCopy() VictimFinder
}

// LRUVictimFinder evicts the least recently used block to evict
type LRUVictimFinder struct {
	LRUQueues [][]int
}

func (e *LRUVictimFinder) MakeCopy() VictimFinder {
	return NewLRUVictimFinder()
}

// NewLRUVictimFinder returns a newly constructed lru evictor
func NewLRUVictimFinder() *LRUVictimFinder {
	e := new(LRUVictimFinder)
	return e
}

func (e *LRUVictimFinder) Reset(sets []Set, numSets int, numWays int) {
	e.LRUQueues = make([][]int, len(sets))
	for _, s := range sets {
		sid := s.Blocks[0].SetID
		e.LRUQueues[sid] = make([]int, numWays)
		for k, b := range s.Blocks {
			e.LRUQueues[sid][k] = b.WayID
		}
	}
}
func (e *LRUVictimFinder) NotifyVictim(block *Block) {
	// NOTHING
}
func (e *LRUVictimFinder) NotifyVisit(block *Block) {
	lruQueue := e.LRUQueues[block.SetID]
	for i, b := range lruQueue {
		if b == block.WayID {
			e.LRUQueues[block.SetID] = append(lruQueue[:i], lruQueue[i+1:]...)
			break
		}
	}
	e.LRUQueues[block.SetID] = append(e.LRUQueues[block.SetID], block.WayID)
}

// FindVictim returns the least recently used block in a set
func (e *LRUVictimFinder) FindVictim(set *Set) *Block {
	matchs := true
	for i, b := range e.LRUQueues[set.Blocks[0].SetID] {
		matchs = matchs && (b == set.LRUQueue[i].WayID)
	}
	if !matchs {
		fmt.Println("ERROR: We don't manage this right....")
	}

	lruQueue := e.LRUQueues[set.Blocks[0].SetID]

	// First try evicting an empty block
	for _, b := range lruQueue {
		if !set.Blocks[b].IsValid && !set.Blocks[b].IsLocked {
			return set.Blocks[b]
		}
	}

	for _, b := range lruQueue {
		if !set.Blocks[b].IsLocked {
			return set.Blocks[b]
		}
	}

	return set.Blocks[lruQueue[0]]
}

/*
*
*
*
*
*
*
*
*
*
 */
type MRUVictimFinder struct {
	MRUQueues [][]int
}

func (e *MRUVictimFinder) MakeCopy() VictimFinder {
	return NewMRUVictimFinder()
}

func NewMRUVictimFinder() *MRUVictimFinder {
	return new(MRUVictimFinder)
}

func (e *MRUVictimFinder) Reset(sets []Set, numSets int, numWays int) {
	e.MRUQueues = make([][]int, len(sets))
	for i, s := range sets {
		e.MRUQueues[i] = make([]int, numWays)
		for k, b := range s.Blocks {
			e.MRUQueues[i][k] = b.WayID
		}
	}
}
func (e *MRUVictimFinder) NotifyVictim(block *Block) {
	// NOTHING
}
func (e *MRUVictimFinder) NotifyVisit(block *Block) {
	mruQueue := e.MRUQueues[block.SetID]
	for i, b := range mruQueue {
		if b == block.WayID {
			e.MRUQueues[block.SetID] = append(mruQueue[:i], mruQueue[i+1:]...)
			break
		}
	}
	e.MRUQueues[block.SetID] = append([]int{block.WayID}, mruQueue...)
}

// FindVictim returns the most recently used block in a set
func (e *MRUVictimFinder) FindVictim(set *Set) *Block {
	mruQueue := e.MRUQueues[set.Blocks[0].SetID]
	// First try evicting an empty block
	for _, b := range mruQueue {
		if !set.Blocks[b].IsValid && !set.Blocks[b].IsLocked {
			return set.Blocks[b]
		}
	}

	for _, b := range mruQueue {
		if !set.Blocks[b].IsLocked {
			return set.Blocks[b]
		}
	}

	return set.Blocks[mruQueue[0]]
}

/*
*
*
*
*
*
*
*
*
*
*
*
*
*
*
 */
type RandomVictimFinder struct {
}

func (e *RandomVictimFinder) MakeCopy() VictimFinder {
	return NewRandomVictimFinder()
}

func NewRandomVictimFinder() *RandomVictimFinder {
	rand.Seed(time.Now().UnixNano())
	return new(RandomVictimFinder)
}
func (e *RandomVictimFinder) Reset(sets []Set, numSets int, numWays int) {
	// NOTHING
}
func (e *RandomVictimFinder) NotifyVictim(block *Block) {
	// NOTHING
}
func (e *RandomVictimFinder) NotifyVisit(block *Block) {
	// NOTHING
}

func (e *RandomVictimFinder) FindVictim(set *Set) *Block {
	// First try evicting an empty block
	for _, block := range set.Blocks {
		if !block.IsValid && !block.IsLocked {
			return block
		}
	}

	// Construct the list of blocks to randomly select from
	var validBlocks []*Block
	for _, block := range set.Blocks {
		if !block.IsLocked {
			validBlocks = append(validBlocks, block)
		}
	}
	if len(validBlocks) > 0 {
		ind := rand.Intn(len(validBlocks))
		return validBlocks[ind]
	}

	return set.Blocks[0]
}

/*
*
*
*
*
*
*
*
*
*
 */
type LFUVictimFinder struct {
	Freqs [][]int
}

func (e *LFUVictimFinder) MakeCopy() VictimFinder {
	return NewLFUVictimFinder()
}

func NewLFUVictimFinder() *LFUVictimFinder {
	return new(LFUVictimFinder)
}
func (e *LFUVictimFinder) Reset(sets []Set, numSets int, numWays int) {
	e.Freqs = make([][]int, numSets)
	for i, _ := range sets {
		e.Freqs[i] = make([]int, numWays)
	}
}
func (e *LFUVictimFinder) NotifyVictim(block *Block) {
	e.Freqs[block.SetID][block.WayID] = 0
}
func (e *LFUVictimFinder) NotifyVisit(block *Block) {
	e.Freqs[block.SetID][block.WayID] += 1
}

func (e *LFUVictimFinder) FindVictim(set *Set) *Block {
	// First try evicting an empty block
	for _, block := range set.Blocks {
		if !block.IsValid && !block.IsLocked {
			return block
		}
	}

	setID := set.Blocks[0].SetID
	min := e.Freqs[setID][set.Blocks[0].WayID]
	minFreqBlock := set.Blocks[0]
	for _, block := range set.Blocks {
		if !block.IsLocked && min >= e.Freqs[setID][block.WayID] {
			min = e.Freqs[setID][block.WayID]
			minFreqBlock = block
		}
	}

	return minFreqBlock
}

/*
*
*
*
*
*
*
*
 */
type MFUVictimFinder struct {
	Freqs [][]int
}

func (e *MFUVictimFinder) MakeCopy() VictimFinder {
	return NewMFUVictimFinder()
}

func NewMFUVictimFinder() *MFUVictimFinder {
	return new(MFUVictimFinder)
}
func (e *MFUVictimFinder) Reset(sets []Set, numSets int, numWays int) {
	e.Freqs = make([][]int, numSets)
	for i, _ := range sets {
		e.Freqs[i] = make([]int, numWays)
	}
}
func (e *MFUVictimFinder) NotifyVictim(block *Block) {
	e.Freqs[block.SetID][block.WayID] = 0
}
func (e *MFUVictimFinder) NotifyVisit(block *Block) {
	e.Freqs[block.SetID][block.WayID] += 1
}

func (e *MFUVictimFinder) FindVictim(set *Set) *Block {
	// First try evicting an empty block
	for _, block := range set.Blocks {
		if !block.IsValid && !block.IsLocked {
			return block
		}
	}

	setID := set.Blocks[0].SetID
	max := 0
	maxFreqBlock := set.Blocks[0]
	for _, block := range set.Blocks {
		if !block.IsLocked && max <= e.Freqs[setID][block.WayID] {
			max = e.Freqs[setID][block.WayID]
			maxFreqBlock = block
		}
	}

	return maxFreqBlock
}

/*
*
*
*
*
*
*
*
 */
type LRUKVictimFinder struct {
	Time             int32
	K                int32
	BlockAccessTimes [][][]int32
}

func (e *LRUKVictimFinder) MakeCopy() VictimFinder {
	return NewLRUKVictimFinder(e.K)
}

func NewLRUKVictimFinder(K int32) *LRUKVictimFinder {
	finder := new(LRUKVictimFinder)
	finder.K = K
	return finder
}

func (e *LRUKVictimFinder) Reset(sets []Set, numSets int, numWays int) {
	e.Time = 0

	e.BlockAccessTimes = make([][][]int32, numSets)
	for i := 0; i < numSets; i++ {
		e.BlockAccessTimes[i] = make([][]int32, numWays)
		for k := 0; k < numWays; k++ {
			e.BlockAccessTimes[i][k] = make([]int32, 0)
		}
	}
}

func (e *LRUKVictimFinder) NotifyVictim(block *Block) {
	setInd := block.SetID
	wayInd := block.WayID
	e.BlockAccessTimes[setInd][wayInd] = make([]int32, 0)

	// UPDATE TIME SO THAT WE NEVER OVERFLOW
	maxx := int32(0)
	minn := int32(0)
	for _, se := range e.BlockAccessTimes {
		for _, la := range se {
			for _, ac := range la {
				minn = int32(math.Min(float64(ac), float64(minn)))
				maxx = int32(math.Max(float64(ac), float64(maxx)))
			}
		}
	}

	e.Time = maxx - minn + 1

	for setI, se := range e.BlockAccessTimes {
		for wayI, la := range se {
			for AccI, _ := range la {
				e.BlockAccessTimes[setI][wayI][AccI] = e.BlockAccessTimes[setI][wayI][AccI] - minn
			}
		}
	}

}

func (e *LRUKVictimFinder) NotifyVisit(block *Block) {
	setID := block.SetID
	wayID := block.WayID
	e.BlockAccessTimes[setID][wayID] = append([]int32{e.Time}, e.BlockAccessTimes[setID][wayID]...)
	if len(e.BlockAccessTimes[setID][wayID]) >= int(e.K) {
		e.BlockAccessTimes[setID][wayID] = e.BlockAccessTimes[setID][wayID][:e.K]
	}
	e.Time += 1
}

// Returns true iff a is a better LRUK victim than b
func (e *LRUKVictimFinder) LRUK_IsBetterVictimThan(a []int32, b []int32) bool {
	if len(a) != len(b) {
		return len(a) < len(b)
	}

	return a[len(a)-1] <= b[len(b)-1]
}

func (e *LRUKVictimFinder) FindVictim(set *Set) *Block {
	// First try evicting an empty block
	for _, block := range set.Blocks {
		if !block.IsValid && !block.IsLocked {
			return block
		}
	}

	bestInd := 0
	KAccessTimes := e.BlockAccessTimes[set.Blocks[0].SetID]
	for ind, block := range set.Blocks {
		if !block.IsLocked && e.LRUK_IsBetterVictimThan(KAccessTimes[ind], KAccessTimes[bestInd]) {
			bestInd = ind
		}
	}

	return set.Blocks[bestInd]
}
